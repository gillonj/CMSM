# -*- coding: utf-8 -*-
"""
Created on Fri May  3 10:13:56 2024

@author: tobia
"""


import scipy.optimize as sop
import numpy as np
from test_few_spins_2D import MySystem

import time
 
a = 2
n = 4                              #lattice constant, arbitrairy length
N = n**2                            #number of spins
                      
J = 1                               #magnitude, exchange interaction between nearest-neigh's       


dmi_mag = 2                       #magnitude of DMI interaction, only give this, the direction is hardcoded in the Hamiltonian

H_mag =  0.5                        # magnitudeof external field 
H_field = H_mag*np.array([0,0,-1])  # will define direction of skyrmion, stabilizes skyrmion 
                                    # should be [0,0,1] or [0,0,-1]
                                    
K = 0                            #magnitnude, (magneto-crystalline) anisotropy
easy_ax = np.array([0,0,1])         #Easy axis (System minimizes energy by aligning spins to this axis)

I = MySystem(N, a, J, K, dmi_mag, H_field, easy_ax)
I.Setup_2D(set_type= "few_in_FM",r=0) #Setup 1 downspin in a chain of upspins
I.matshow()
I.minimize()
I.matshow()
I.savedata('test')


x0 = I.s0       #initial config of spins, defined by Setup_1D
#x0[2] +=0.001   #would be trapped in local minima

t0 = time.time()
ret = sop.minimize(I.Hamiltonian_pbc, x0, tol = 1e-3)
print("Einitial config\n" ,I.Hamiltonian_pbc(x0))
print("E final config\n" ,ret.fun)
print(np.round(time.time()-t0), "seconds used")

##------------

I.plot_config2D(ret.x)
