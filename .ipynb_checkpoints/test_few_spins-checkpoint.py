# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 15:27:04 2024

@author: tobia
"""
import numpy as np
<<<<<<< HEAD
import scipy.optimize as sop
=======

import matplotlib.pyplot as plt 
>>>>>>> main

class MySystem:

    def __init__(self,N,a,J,K,dmi,easy_ax):
<<<<<<< HEAD
=======
        """
        Setting up the "global" variables as defined in another file. Inputed by running MySystem(N,a,J,...).
        What this allows for, that any function of this class (e.g. Setup_1D) can always access all self.XYZ variables
        ----------
        N : int
            number of spins.
        a : float
            lattice constant, arbitrairy length.
        J : float
            magnitude, exchange interaction between nearest-neigh's.
        K : 1D array
            (magneto-crystalline) anisotropy (mca), magnitude and direction (array).
        dmi : 1D array
            Dzyaloshinskii–Moriya interaction, magnitude and direction (array).
        easy_ax : 1D array
            Defines the direction (array) of the easy axis, where the mca reduces the energy.

        Returns
        -------
        Methods to simulate interaction of effective, classical spins according to a 3D Heisenberg Hamiltonian. 
        Find relaxed confugrations using scipy.optimize algs.

        """
        
>>>>>>> main
        np.random.seed(1)
        self.a = a
        self.N = N
        self.J = J
        self.K = K
        self.dmi = dmi
        self.easy_ax = easy_ax
<<<<<<< HEAD
        
    def Setup_1D(self,set_type,with_pbc):
        if with_pbc:
            #create ghost cells
            self.spins = np.zeros((self.N+2,3)) #x,y,z components of spin, moduli must equal 1
        else:
            self.spins = np.zeros((self.N,3)) 
            
        self.loc = self.a*np.linspace(0,self.N-1,self.N)
        if set_type == "1_in_FM":
            if with_pbc:
                self.spins[1:-1,2] = 1
            else:
                self.spins[:,2] = 1
            self.spins[int(self.N/2),2] = -1
        else:
            print("Error! set_type not known!")
            
    def Hamiltonian(self):
        self.H = np.zeros(self.N)
        for i in range(self.N):
            E_ex_dmi = 0
            for j in range(i+1,self.N):
                E_ex_dmi += -self.J*np.dot(self.spins[i,:],self.spins[j,:]) + np.dot(self.dmi,np.cross(self.spins[i,:],self.spins[j,:]))
            
            E_mca = -self.K*np.dot(self.easy_ax,self.spins[i,:])
            self.H[i] = E_ex_dmi+E_mca
     #new input
=======
        self.n = int(self.N**(1/2))
        
    def Setup_1D(self,set_type):
        """

        Parameters
        ----------
        set_type : str
            Sets type of initial confugration  of the N spins.
            
            1_in_FM: 1 spin "up" in a chain of spin "down"
            
            FM: all spins collinear

        Returns
        -------
        First building an array of N*2 spins, i.e. the inital configuration. Each spin has the two angular values theta & phi
        needed to define a spin in spherical coordinates.
        In the end the ravel function, reshapes the matrix into a 1D array, alternating with theta phi theta phi...
        values, since scipy.optimize algorithms do not understand matrices, but only 1d arrays.
        
        The initial configuration according to set_type.

        """

        self.spins = np.zeros((self.N,2)) 
        self.loc = np.zeros((3,self.N) )
        self.loc[0,:] = self.a*np.linspace(0,self.N-1,self.N)
        if set_type == "1_in_FM":
            self.spins[:,0] = 0
            self.spins[int(self.N/2),0] = np.pi
        elif set_type == "FM":
            self.spins[:,0] = np.pi/2
        else:
            print("Error! set_type not known!")
        self.spins = self.spins.ravel() #optim solver can only handle 1D array
        self.s0 = self.spins
        
    def Setup_2D(self,set_type):
        """
        

        Parameters
        ----------
        set_type : TYPE
            DESCRIPTION.

        Returns
        -------
        self.n is the square root of self.N (rounded if necessary)

        """
        
        self.spins = np.zeros((2,self.n,self.n)) 
        
        xy = np.mgrid[0:self.n,0:self.n] #x= loc[0], y = loc[1]
        z = np.zeros((1,self.n,self.n))
        self.loc  = np.concatenate((xy,z))
        
        if set_type == "1_in_FM":
            self.spins[0,:,:] = 0
            self.spins[0,int(self.n/2),int(self.n/2)] = np.pi
        elif set_type == "FM":
            self.spins[:,:,0] = 0
        else:
            print("Error! set_type not known!")
        self.spins = self.spins.ravel() #optim solver can only handle 1D array
        #@Anouk, to go back from raveled, to matrix form use: self.spins.reshape((2,self.n,self.n))
        # self.spins[0] are the theta values and self.spins[1] the phi values. e.g. self.spins[:][1][1] interact
        #with [0][1], [1][0], [1][2] and [2][1] (right?)
        self.s0 = self.spins
            
    def sph_to_car(self,spins, m=1):
        """
        

        Parameters
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).
        m : float, optional
            Magnetization can be entered (magnitude of effective spin). The default is 1.

        Returns
        -------
        car_coord : N*3 array
            Cartesian coordinates, each row are the x,y,z components in this order.

        """
        N = int(len(spins)/2)
        
        car_coord = np.zeros((N,3))
        theta = spins[::2] #all even elements of array are the theta values
        phi = spins[1::2] #all odd elements the phi values
        car_coord[:,0] = m*np.sin(theta)*np.cos(phi) #x = r*sin(theta)*cos(phi)
        car_coord[:,1] = m*np.sin(theta)*np.sin(phi) #y
        car_coord[:,2] = m*np.cos(theta) #z
        
        
        return car_coord
    
    def sph_to_car2D(self,spins, m=1):
        """
        

        Parameters
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).
        m : float, optional
            Magnetization can be entered (magnitude of effective spin). The default is 1.

        Returns
        -------
        car_coord : N*3 array
            Cartesian coordinates, each row are the x,y,z components in this order.
            to obtain a matrix in cartesian coordinates use:
            xyz_mat = car_coord.T.reshape((3,self.n,self.n))
            to select spin (i,j) do xyz_mat[:,i,j]
        """
    
        mat = self.spins.reshape((2,self.n,self.n))
        car_coord = np.zeros((self.n**2,3))
        theta = mat[0].ravel() #all even elements of array are the theta values
        phi = mat[1].ravel() #all odd elements the phi values
        car_coord[:,0] = m*np.sin(theta)*np.cos(phi) #x = r*sin(theta)*cos(phi)
        car_coord[:,1] = m*np.sin(theta)*np.sin(phi) #y
        car_coord[:,2] = m*np.cos(theta) #zn
        
        #to obtain a matrix in cartesian coordinates use:
        #xyz_mat = car_coord.T.reshape((3,self.n,self.n))
        #to select spin (i,j) do xyz_mat[:,i,j]
        
        return car_coord
            
    def Hamiltonian_obc(self,spins):
        """
        1D Heisenberg Hamiltonian with open boundary conditions (each spin at the end has only 1 neighbor).
        Parameters
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).

        Returns
        -------
        E : float
            Energy from evaluating the full Hamiltonian as given in Huang et all (22). 
            Depends on the given J,K and dmi (check via self.J or self.dmi, etc)
            
            Only evaluates the nearest neighbors
            

        """
        
        self.H = np.zeros(self.N)
        xyz = np.zeros((self.N+2,3)) #length of N+2 to have ghost cells (xyz = [0,0,0]), so that the first and last spin have an "empty" neighbor.
        xyz[1:-1,:] = self.sph_to_car(spins) #Obtain cartesian coordinates to evaluate the Hamiltonian as given in the paper
        for i in range(1,self.N+1):
            
                
            E_ex_dmi = -self.J*np.dot(xyz[i,:],xyz[i+1,:]) -self.J*np.dot(xyz[i,:],xyz[i-1,:])
            + np.dot(self.dmi,np.cross(xyz[i,:],xyz[i+1,:])) + np.dot(self.dmi,np.cross(xyz[i,:],xyz[i-1,:]))
            
            E_mca = -self.K*np.dot(self.easy_ax,xyz[i,:])
            
            self.H[i-1] = E_ex_dmi+E_mca
        E = np.sum(self.H)
        
        return E
    
    def plot_config(self,spins):
        """
        Plotting the initial and possibly the relaxed configuration of spins. 
        Using the matplotlib quiver function.
        
        Parameters
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).
            If the input spins (after relaxation) are the same as the initial configuration (saved as self.s0)
            only the initial configuarion will be plotted (since nothing changed or no relaxation was done.)

        Returns
        -------
        Figures, up to two plots.

        """
        
        #fig = plt.figure()
        
        ax = plt.figure(dpi=400).add_subplot(projection='3d')
        xyz = self.sph_to_car(self.s0)
        x,y,z = self.loc

        
        
        # Directional vectors 
        u = xyz[:,0]
        v = xyz[:,1] 
        w = xyz[:,2]
        #2D plot, neglecting z component
          
        # Plotting Vector Field with QUIVER 
        ax.quiver(x, y,z, u, v,w, color='r') 
        plt.title('Initial Configuration') 
          
        # Setting x, y boundary limits 
        ax.set_xlim3d(0, 2*(self.N+1)) 
        ax.set_ylim3d(-1,1) 
        ax.set_zlim3d(-1,1)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
          
        # Show plot with grid 
        ax.grid() 
        plt.show() 
        if (spins != self.s0).any():
            ax = plt.figure(dpi=400).add_subplot(projection='3d')
            xyz = self.sph_to_car(spins)
    
            u = xyz[:,0]
            v = xyz[:,1] 
            w = xyz[:,2]
    
            ax.quiver(x, y,z, u, v,w, color='b') 
            plt.title('Relaxed Configuration') 
    
            ax.set_xlim3d(0, 2*(self.N+1)) 
            ax.set_ylim3d(-1,1) 
            ax.set_zlim3d(-1,1)
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
        
            plt.grid() 
            plt.show() 
    
                

            

>>>>>>> main
