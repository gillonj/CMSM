
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 13 15:27:04 2024

@author: tobia
"""
import numpy as np
import scipy.optimize as sop
import matplotlib.pyplot as plt 
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
import os
import time

class MySystem:

    def __init__(self,N,a,J,K,dmi_mag, H_field, easy_ax):
        """
        Setting up the "global" variables as defined in another file. Inputed by running MySystem(N,a,J,...).
        What this allows for, that any function of this class (e.g. Setup_1D) can always access all self.XYZ variables
        ----------
        N : int
            number of spins.
        a : float
            lattice constant, arbitrairy length.
        J : float
            magnitude, exchange interaction between nearest-neigh's.
        K : 1D array
            (magneto-crystalline) anisotropy (mca), magnitude and direction (array).
        dmi : 1D array
            Dzyaloshinskii–Moriya interaction, magnitude and direction (array).
        easy_ax : 1D array
            Defines the direction (array) of the easy axis, where the mca reduces the energy.

        Returns
        -------
        Methods to simulate interaction of effective, classical spins according to a 3D Heisenberg Hamiltonian. 
        Find relaxed confugrations using scipy.optimize algs.

        """
        
        np.random.seed(1)
        self.a = a
        self.N = N
        self.J = J
        self.K = K
        self.dmi_mag = dmi_mag
        self.easy_ax = easy_ax
        self.n = int(self.N**(1/2))
        self.H_field = H_field
        
   
    def Setup_2D(self,set_type,r=2):
        """
        

        Parameters
        ----------
        set_type : TYPE
            DESCRIPTION.

        Returns
        -------
        self.n is the square root of self.N (rounded if necessary)

        """
        
        self.spins = np.zeros((2,self.n,self.n)) 
        
        xy = np.mgrid[0:self.n,0:self.n] #x= loc[0], y = loc[1]
        z = np.zeros((1,self.n,self.n))
        self.loc  = np.concatenate((xy,z))
        
        if set_type == "1_in_FM":
            self.spins[0,:,:] = 0
            self.spins[0,int(self.n/2),int(self.n/2)] = np.pi
        elif set_type == "few_in_FM":
            self.spins[0,:,:] = 0
            print("radius used:", r)
            m = int(self.n/2)
            for i in range(-r,r+1):
                for j in range(-r,r+1):
                    rsq = i**2 + j**2
                    if rsq <= r**2:
                        self.spins[0,m+i,m+j] = np.pi
                        
        elif set_type == "FM":
            self.spins[:,:,0] = 0
        else:
            print("Error! set_type not known!")
        
        self.spins = self.spins.ravel() #optim solver can only handle 1D array
        #@Anouk, to go back from raveled, to matrix form use: self.spins.reshape((2,self.n,self.n))
        # self.spins[0] are the theta values and self.spins[1] the phi values. e.g. self.spins[:][1][1] interact
        #with [0][1], [1][0], [1][2] and [2][1] (right?)
        
        self.s0 = self.spins

    
    def sph_to_car2D(self,spins, m=1):
        """
        Parameters
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).
        m : float, optional
            Magnetization can be entered (magnitude of effective spin). The default is 1.

        Returns
        -------
        car_coord : N*3 array
            Cartesian coordinates, each row are the x,y,z components in this order.
            to obtain a matrix in cartesian coordinates use:
            xyz_mat = car_coord.T.reshape((3,self.n,self.n))
            to select spin (i,j) do xyz_mat[:,i,j]
        """
    
        mat = spins.reshape((2,self.n,self.n))

        car_coord = np.zeros((self.n**2,3))
        theta = mat[0].ravel() #all even elements of array are the theta values
        phi = mat[1].ravel() #all odd elements the phi values
        car_coord[:,0] = m*np.sin(theta)*np.cos(phi) #x = r*sin(theta)*cos(phi)
        car_coord[:,1] = m*np.sin(theta)*np.sin(phi) #y
        car_coord[:,2] = m*np.cos(theta) #zn
        
        #to obtain a matrix in cartesian coordinates use:
        #xyz_mat = car_coord.T.reshape((3,self.n,self.n))
        #to select spin (i,j) do xyz_mat[:,i,j]
        
        return car_coord

            
#Hamiltonian in 2D           
        
    def Hamiltonian_pbc(self,spins):

        """
        2D Heisenberg Hamiltonian with periodic boundary conditions
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).

        Returns
        -------
        E : float
            Energy from evaluating the full Hamiltonian as given in Huang et all (22). 
            Depends on the given J,K and dmi (check via self.J or self.dmi, etc)
            
            Only evaluates the nearest neighbors (top,bottom,left,right)
        """
               #periodic PBC


        def pbc(i,dim):  #need input of dim of Matrix
            if i>=dim:

                i=i-dim
            elif i<0:
                i=i+dim
            return i


        H = np.zeros((self.n,self.n))   #hamiltoian energy of each spin,saved in i,j Matrix of size n^2
        #xyz_mat = np.zeros((3,self.n,self.n))  
        car_coord = self.sph_to_car2D(spins) #Obtain cartesian coordinates 
        xyz_mat = car_coord.T.reshape((3,self.n,self.n)) #reshape cartesian coordinates to 3D matrix with 3 planes (x,y,z) describing the spin vectors and each plane having n i,j component describing the position.
        
        for i in range(0,self.n):
            for j in range(0,self.n):
                
            #spin at position i,j can be found through: xyz_mat[:,i,j]
                  
                n1 = -self.J*np.dot(xyz_mat[:,i,j],np.transpose(xyz_mat[:,pbc(i+1,self.n),j])) 
                n2 = -self.J*np.dot(xyz_mat[:,i,j],np.transpose(xyz_mat[:,pbc(i-1,self.n),j]))
                n3 = -self.J*np.dot(xyz_mat[:,i,j],np.transpose(xyz_mat[:,i,pbc(j+1,self.n)])) 
                n4 = -self.J*np.dot(xyz_mat[:,i,j],np.transpose(xyz_mat[:,i,pbc(j-1,self.n)]))
        
            #print("neigh:", [i,j],"with", [n1,n2,n3,n4])
        

                dmi1 = np.dot(self.dmi_mag*np.array([-1,0,0]),np.cross(xyz_mat[:,i,j],xyz_mat[:,pbc(i+1,self.n),j])) 
                dmi2 = np.dot(self.dmi_mag*np.array([1,0,0]),np.cross(xyz_mat[:,i,j],xyz_mat[:,pbc(i-1,self.n),j])) 
                dmi3 = np.dot(self.dmi_mag*np.array([0,1,0]),np.cross(xyz_mat[:,i,j],xyz_mat[:,i,pbc(j+1,self.n)])) 
                dmi4 =  np.dot(self.dmi_mag*np.array([0,-1,0]),np.cross(xyz_mat[:,i,j],xyz_mat[:,i,pbc(j-1,self.n)]))
                
                E_ex_dmi = 0.5*(n1+n2+n3+n4 +dmi1 + dmi2 + dmi3 + dmi4)         #0.5 accounts for double counting of neighbour interaction
                
                E_mca = -self.K*np.dot(self.easy_ax,xyz_mat[:,i,j])
                
                E_Hfield = -np.dot(self.H_field,xyz_mat[:,i,j])
                
                Eij = E_ex_dmi + E_mca + E_Hfield
                
                H[i,j] = Eij
       
        #print('H', H)
        E = np.sum(H)
        #print('current E=', E)
    
        return E
        
        
 ##### HERE STUFF FROM 1D       
    def plot_config3D(self,spins):
        """
        Plotting the initial and possibly the relaxed configuration of spins. 
        Using the matplotlib quiver function.
        
        Parameters
        ----------
        spins : 1d array 
            Array contains angles theta & phi (alternating).
            If the input spins (after relaxation) are the same as the initial configuration (saved as self.s0)
            only the initial configuarion will be plotted (since nothing changed or no relaxation was done.)

        Returns
        -------
        Figures, up to two plots.

        """
        
        #fig = plt.figure()
        
        ax = plt.figure(dpi=400).add_subplot(projection='3d')
        xyz = self.sph_to_car2D(self.s0)
        #xyz_mat = xyz.T.reshape((3,self.n,self.n))
        #plt.matshow(xyz_mat[2])
        #plt.show()
        x,y,z = self.loc
        x = x.ravel()   #need array for plotting instead of matrix
        y = y.ravel()
        z = z.ravel()

        """ If working with spherical coordinates for the colour scheme
        r = np.sqrt(x**2 + y**2 + z**2)
        theta = np.empty_like(r)
        for i in range(len(r)):
            if r[i] == 0:
                theta[i] = 0
            else:  
                theta[i] = np.arccos(z[i] / r[i])          
        phi = np.arctan2(y, x)
        """
   
        # Directional vectors 
        u = xyz[:,0]
        v = xyz[:,1] 
        w = xyz[:,2]


        #ac = np.hypot(u, v) #can do color scheme with combination of angles
        
        angle_color = plt.cm.viridis(w) #color based off of directional vector w
       
          
        # Plotting Vector Field with QUIVER 
        ax.quiver(x, y, z, u, v, w, pivot = 'middle', color = angle_color) 
        plt.title('Initial Configuration') 
          
        # Setting x, y boundary limits 
        ax.set_xlim3d(0, (self.n+1)) 
        ax.set_ylim3d(0, (self.n+1)) 
        ax.set_zlim3d(-1,1)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
          
        # Show plot with grid 
        ax.grid() 
        plt.show() 
        if (spins != self.s0).any():
            ax = plt.figure(dpi=400).add_subplot(projection='3d')
            xyz = self.sph_to_car2D(spins)
    
            u = xyz[:,0]
            v = xyz[:,1] 
            w = xyz[:,2]
            #arrow head fix (?)
            w_color = (w.ravel()-w.min())/w.ptp()
            w_color = np.concatenate((w_color, np.repeat(w_color,2)))

            #ac = np.hypot(u, v) #can do color scheme with combination of angles

            angle_color = plt.cm.viridis(w_color) #color based off of directional vector w
    
            ax.quiver(x, y, z, u, v, w, pivot = 'middle', color=angle_color) 
            plt.title('Relaxed Configuration') 
    
            ax.set_xlim3d(0, (self.n+1)) 
            ax.set_ylim3d(0, (self.n+1)) 
            ax.set_zlim3d(-1,1)
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
        
            plt.grid() 
            plt.show()

    def plot_config2D(self,spins):
        plt.rcParams['font.size'] = 3
        ax = plt.figure(dpi=400, figsize=(2,2)).add_subplot()
        
        xyz = self.sph_to_car2D(self.s0)
        #xyz_mat = xyz.T.reshape((3,self.n,self.n))
        #plt.matshow(xyz_mat[2])
        #plt.show()
        x,y,z = self.loc
        x = x.ravel()   #need array for plotting instead of matrix
        y = y.ravel()
        z = z.ravel()

        """ If working with spherical coordinates for the colour scheme
        r = np.sqrt(x**2 + y**2 + z**2)
        theta = np.empty_like(r)
        for i in range(len(r)):
            if r[i] == 0:
                theta[i] = 0
            else:  
                theta[i] = np.arccos(z[i] / r[i])          
        phi = np.arctan2(y, x)
        """
   
        # Directional vectors 
        u = xyz[:,0]
        v = xyz[:,1] 
        w = xyz[:,2]

        #colormapping    
        cm = plt.cm.viridis  #(w) #color based off of directional vector w
        norm = mpl.colors.Normalize()
        norm.autoscale(w)
        #scalar mappable
        sm = mpl.cm.ScalarMappable(cmap=cm, norm=norm)
        sm.set_array([])
          
        # Plotting Vector Field with QUIVER 
        ax.quiver(x, y, u, v, pivot = 'middle', color = cm(norm(w)), scale_units='xy', scale=1) 
        plt.title('Initial Configuration') 
          
        # Setting x, y boundary limits 
        ax.set_xlim(-1, (self.n)) 
        ax.set_ylim(-1, (self.n)) 
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        

        #info for colorbar
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="2%", pad=0.05)
          
        # Show plot with grid 
        ax.grid(linewidth=0.5, color='#DFDFDF')
        ax.set_axisbelow(True) 
        #colorbar
        plt.colorbar(sm, cax=cax)

        #plot
        plt.show() 

        if (spins != self.s0).any():
            ax = plt.figure(dpi=400, figsize=(2,2)).add_subplot()
            xyz = self.sph_to_car2D(spins)
    
            u = xyz[:,0]
            v = xyz[:,1] 
            w = xyz[:,2]

            #colormapping
            cm = plt.cm.viridis  #(w) #color based off of directional vector w
            norm = mpl.colors.Normalize()
            norm.autoscale(w)
            #need scalar mappable for colorbar
            sm = mpl.cm.ScalarMappable(cmap=cm, norm=norm)
            sm.set_array([])
          
            # Plotting Vector Field with QUIVER 
            ax.quiver(x, y, u, v, pivot = 'middle', color = cm(norm(w))) 
            plt.title('Relaxed Configuration') 
    
            ax.set_xlim(-1, (self.n)) 
            ax.set_ylim(-1, (self.n)) 
            ax.set_xlabel('x')
            ax.set_ylabel('y')

            #adjusting grid, this is further up bc otherwise it goes into colorbar???
            plt.grid(visible=True, linewidth=0.5, color='#DFDFDF')
            ax.set_axisbelow(True)

            #info for colorbar
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="2%", pad=0.05)
            cbar = plt.colorbar(sm, cax=cax)
            cbar.ax.tick_params(labelsize=2)

            #plot
            plt.show()

                
