# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 13:21:16 2024

@author: tobia
"""

import scipy.optimize as sop
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib as mpl
from test_few_spins_2D import MySystem
from mpl_toolkits.axes_grid1 import make_axes_locatable
 
a = 2                               #lattice constant, arbitrairy length
N = 36                             #number of spins                   
J = 1                               #magnitude, exchange interaction between nearest-neigh's
K = 0.1                            #magnitnude, (magneto-crystalline) anisotropy
dmi_mag = 2                        #magnitude of DMI interaction, only give this, the direction is hardcoded in the Hamiltonian
#dmi = dmi_mag*np.array([0,1,0])     #vector, orientation of DMI
H_mag =  0.1                        # magnitudeof external field 
H_field = H_mag*np.array([0,0,-1])  # will define direction of skyrmion, stabilizes skyrmion 
                                    # should be [0,0,1] or [0,0,-1]
easy_ax = np.array([0,0,1])         #Easy axis (System minimizes energy by aligning spins to this axis)

I = MySystem(N, a, J, K, dmi_mag, H_field, easy_ax)
I.Setup_2D(set_type= "1_in_FM") #Setup 1 downspin in a chain of upspins



x0 = I.s0       #initial config of spins, defined by Setup_1D
x0[2] +=0.001   #would be trapped in local minima
print("Cartesian coordinates of the initial config: \n")
print(np.round(I.sph_to_car2D(x0)),"\n")

ret = sop.minimize(I.Hamiltonian_pbc, x0, tol = 1e-3)
print("Einitial config\n" ,I.Hamiltonian_pbc(x0))
print("E final config\n" ,ret.fun)

loc = I.loc

def plot_config2D_test(I, spins):
        plt.rcParams['font.size'] = 3
        ax = plt.figure(dpi=400, figsize=(2,2)).add_subplot()
        
        xyz = I.sph_to_car2D(I.s0)
        #xyz_mat = xyz.T.reshape((3,self.n,self.n))
        #plt.matshow(xyz_mat[2])
        #plt.show()
        x,y,z = I.loc
        x = x.ravel()   #need array for plotting instead of matrix
        y = y.ravel()
        z = z.ravel()

        """ If working with spherical coordinates for the colour scheme
        r = np.sqrt(x**2 + y**2 + z**2)
        theta = np.empty_like(r)
        for i in range(len(r)):
            if r[i] == 0:
                theta[i] = 0
            else:  
                theta[i] = np.arccos(z[i] / r[i])          
        phi = np.arctan2(y, x)
        """
   
        # Directional vectors 
        u = xyz[:,0]
        v = xyz[:,1] 
        w = xyz[:,2]

        #colormapping    
        cm = plt.cm.viridis  #(w) #color based off of directional vector w
        norm = mpl.colors.Normalize()
        norm.autoscale(w)
        #scalar mappable
        sm = mpl.cm.ScalarMappable(cmap=cm, norm=norm)
        sm.set_array([])
          
        # Plotting Vector Field with QUIVER 
        ax.quiver(x, y, u, v, pivot = 'middle', color = cm(norm(w)), scale_units='xy', scale=1) 
        plt.title('Initial Configuration') 
          
        # Setting x, y boundary limits 
        ax.set_xlim(-1, (I.n)) 
        ax.set_ylim(-1, (I.n)) 
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        

        #info for colorbar
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="2%", pad=0.05)
          
        # Show plot with grid 
        ax.grid(linewidth=0.5, color='#DFDFDF')
        ax.set_axisbelow(True) 
        #colorbar
        plt.colorbar(sm, cax=cax)

        #plot
        plt.show() 

        if (spins != I.s0).any():
            ax = plt.figure(dpi=400, figsize=(2,2)).add_subplot()
            xyz = I.sph_to_car2D(spins)
    
            u = xyz[:,0]
            v = xyz[:,1] 
            w = xyz[:,2]

            #colormapping
            cm = plt.cm.viridis  #(w) #color based off of directional vector w
            norm = mpl.colors.Normalize()
            norm.autoscale(w)
            #need scalar mappable for colorbar
            sm = mpl.cm.ScalarMappable(cmap=cm, norm=norm)
            sm.set_array([])
          
            # Plotting Vector Field with QUIVER 
            ax.quiver(x, y, u, v, pivot = 'middle', color = cm(norm(w))) 
            plt.title('Relaxed Configuration') 
    
            ax.set_xlim(-1, (I.n)) 
            ax.set_ylim(-1, (I.n)) 
            ax.set_xlabel('x')
            ax.set_ylabel('y')

            #adjusting grid, this is further up bc otherwise it goes into colorbar???
            plt.grid(visible=True, linewidth=0.5, color='#DFDFDF')
            ax.set_axisbelow(True)

            #info for colorbar
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="2%", pad=0.05)
            cbar = plt.colorbar(sm, cax=cax)
            cbar.ax.tick_params(labelsize=2)

            #plot
            plt.show()


plot_config2D_test(I, ret.x)


                    
        
        